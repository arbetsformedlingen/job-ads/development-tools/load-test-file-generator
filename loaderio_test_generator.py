import requests
import json
import random




def import_region_ids():
    '''Fetches regions from Taxonomy rest endpoint'''
    r = requests.get('https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concepts?type=region')
    return create_id_list(r)


def import_municipality_ids():
    '''Fetches municipalities from Taxonomy rest endpoint'''
    r = requests.get('https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concepts?type=municipality')
    return create_id_list(r)

def import_occupation_group_ids():
    '''Fetches occupation groups from taxonomy rest endpoint'''
    r = requests.get('https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/concepts?type=ssyk-level-4')
    return create_id_list(r)


def create_id_list(r):
    data = r.json()
    municipality_ids = []
    for item in data:
        municipality_ids.append([item['taxonomy/id']])
    return municipality_ids

def generate_complete_values(min_chars=1):
    terms_to_complete = ["stockholms län", "göteborg", "personlig assistent", "stockholm", "undersköterska", "lärare",
                         "butikssäljare", "sjuksköterska", "umeå", "lagerarbetare", "malmö", "skåne län",
                         "västra götalands län", "örebro län", "administratör",
                         "nystartsjobb", "rekryteringsutbildning", "distans",
                         "sjukvård", "stockholm nyetablering", "stockholm nystartsjobb"]
    print(f"Creating testvalues for /complete and the terms: \n{terms_to_complete}" )


    return_test_values = []

    for complete_string in terms_to_complete:
        word_list = complete_string.split(' ')
        word_to_complete = word_list[-1]
        complete_words_chars = list(word_to_complete)[0:]

        for i in range(1, len(complete_words_chars)+1):
            if i >= min_chars:
                word_list_result = " ".join(word_list[0:-1]) + " " + "".join(complete_words_chars[0:i])
                word_list_result = word_list_result.strip()
                # print(word_list_result)
                return_test_values.append(word_list_result)
        # Add full string plus one space
        return_test_values.append(complete_string + " ")

    return return_test_values


def generate_offset(limit):
    offset = []
    for x in range(10):
        offset.append([str(x*limit)])
    return offset


def generate_variable_dict(**kwargs):
    '''
    Generates a dictionary with the variables of the key and values sent in. Key should be a the type and values a list of list of string.
    ex: generate_variable_dict(municipality=[['1100'],['0210']])
    '''
    payload = {"version": 1}
    variables = []
    for key, valuelist in kwargs.items():
        variables.append({"names": [key], "values": valuelist})
    payload['variables'] = variables
    return payload


def generate_key_value_dict(size, **kwargs):
    '''
    Generates a dictionary in the loader.io key/value format
    '''
    payload = {}
    param_keys = []
    param_values = []
    for key, _ in kwargs.items():
        param_keys.append(key)
    for i in range(size):
        rand_list = []
        for key, valuelist in kwargs.items():
            v = random.randint(0, len(valuelist)-1)
            rand_list.append(valuelist[v][0])
        param_values.append(rand_list)
    payload['keys'] = param_keys
    payload['values'] = param_values
    return payload

def generate_key_value_dict_for_keys_values(test_keys:list, test_values:list):
    '''
    Generates a dictionary in the loader.io key/value format
    '''
    payload = {}
    param_keys = []
    param_values = []
    for key in test_keys:
        param_keys.append(key)

    for value in test_values:
        param_values.append([value])

    payload['keys'] = param_keys
    payload['values'] = param_values
    return payload


def to_file(j, filename):
    with open(filename, "w", encoding="utf-8") as fp:
        json.dump(j, fp, indent=4)



if __name__ == "__main__":

    muni = import_municipality_ids()
    lan = import_region_ids()
    j_group = import_occupation_group_ids()
    offset_10 = generate_offset(10)
    offset_100 = generate_offset(100)
    to_file(generate_variable_dict(region=lan), "test_variable_region.json")
    to_file(generate_variable_dict(municipality=muni), "test_variable_municipality.json")
    to_file(generate_variable_dict(occupation_group=j_group), "test_variable_occ_group.json")
    to_file(generate_variable_dict(region=lan, group=j_group, offset=offset_100), "test_bulk_variable_region_occ_group.json")
    to_file(generate_variable_dict(municipality=muni, group=j_group, offset=offset_10), "test_variable_municipality_occ_group.json")
    to_file(generate_key_value_dict(1000, municipality=muni, group=j_group), "test_key_value_municipality_occ_group_1000.json")
    to_file(generate_key_value_dict(1000, region=lan, group=j_group), "test_key_value_region_occ_group_1000.json")
    to_file(generate_key_value_dict(1000, municipality=muni), "test_key_value_municipality_1000.json")
    to_file(generate_key_value_dict_for_keys_values(test_keys=['q'], test_values=generate_complete_values(min_chars=2)), "test_key_value_label_complete_min_2_chars.json")
    to_file(generate_key_value_dict_for_keys_values(test_keys=['q'], test_values=generate_complete_values(min_chars=6)), "test_key_value_label_complete_min_6_chars.json")
    to_file(generate_key_value_dict_for_keys_values(test_keys=['q'], test_values=generate_complete_values(min_chars=2)), "test_key_value_complete.json")
